"""main.py"""
import logging
import io
import os
import time
from google.cloud import storage
from PIL import Image


def timer(func):
    def timer(*args, **kwargs):
        t1 = time.time()
        result = func(*args, **kwargs)
        t2 = time.time()
        logging.info("Executed %s in %.4f seconds" % (func.__name__, (t2 - t1)))
        return result

    return timer


@timer
def configure_logging():
    logging.basicConfig()
    logging.getLogger().setLevel(logging.INFO)


@timer
def parse_event(event):
    file_name = event.get("name")
    bucket = event.get("bucket")

    return file_name, bucket


@timer
def get_gcs_utils(file_name, bucket_name):
    client = storage.Client()
    bucket = client.get_bucket(bucket_name)
    blob = bucket.get_blob(file_name)

    return client, bucket, blob


@timer
def get_bytes_from_gcs(blob):
    file_bytes = blob.download_as_bytes()
    return io.BytesIO(file_bytes)


@timer
def resize_image(file_bytes):
    image = Image.open(file_bytes)
    resized_image = image.resize((400, 400))
    img_bytes = io.BytesIO()
    resized_image.save(img_bytes, format="JPEG")
    img_bytes.seek(0)
    return img_bytes


@timer
def upload_resized_to_gcs(img_bytes, client):
    target_bucket_name = os.environ.get("TARGET_BUCKET")
    target_bucket = client.get_bucket(target_bucket_name)
    target_file_name = "resized.jpeg"
    resized_img_blob = target_bucket.blob(blob_name)
    resized_img_blob.upload_from_file(img_bytes)


@timer
def cloud_fn(event, context):
    """Triggered by a change to a Cloud Storage bucket.
    Args:
         event (dict): Event payload.
         context (google.cloud.functions.Context): Metadata for the event.
    """
    configure_logging()

    file_name, bucket_name = parse_event(event)
    client, bucket, blob = get_gcs_utils(file_name, bucket_name)

    original_bytes = get_bytes_from_gcs(blob)
    resized_bytes = resize_image(original_bytes)

    upload_resized_to_gcs(resized_bytes, client)
